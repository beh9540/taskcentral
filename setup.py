from setuptools import setup

setup(name='TaskCentral',
      version='0.1',
      description='App for a Distributed Task List',
      author='Blake Howell',
      author_email='bhowell@mikotheplot.com',
      url='http://www.python.org/sigs/distutils-sig/',
#      install_requires=['Django>=1.3'],
     )
