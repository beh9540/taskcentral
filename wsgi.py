import os
import sys
import bottle
# Change working directory so relative paths (and template lookup) work again
os.chdir(os.path.dirname(__file__))

import sys
print sys.path

from server.rest import task_app
# ... build or import your bottle application here ...
# Do NOT use bottle.run() with mod_wsgi
application = task_app