import os
from bottle import route, run, request, abort, Bottle, static_file
from pymongo import MongoClient
from bson import ObjectId
from bson.json_util import dumps
import logging

# Setup Mongo
conn = MongoClient(os.getenv('IP'))
db = conn.taskcentral

# Setup logging
logging.basicConfig(filename=os.path.join('log', 'taskcentral.log'))
logger = logging.getLogger(__name__)

# Setup new filter
def id_filter(config):
    regexp = r'[a-f0-9]{24}'

    def to_python(match):
        return ObjectId(match)

    def to_url(numbers):
        return str(numbers)

    return regexp, to_python, to_url

# setup task app
task_app = Bottle()
task_app.router.add_filter('id', id_filter)

def _serialize_task(task_obj):
    task_id = task_obj["_id"]
    task_obj["_id"] = str(task_id)
    return task_obj

@task_app.route('/')
def get_index():
    return static_file('html/index.html', root='web/')

@task_app.route('/task/', method='GET')
@task_app.route('/task/<id:id>/', method='GET')
def get_tasks(id=None):
    tasks = db.tasks
    if id:
        task = tasks.find_one({"_id": ObjectId(id)})
        if task:
            return _serialize_task(task)
        else:
            abort(404, 'No task found')
    else:
        task_list = []
        for task in tasks.find():
            task_json = _serialize_task(task)
            task_list.append(task_json)
        return dumps(task_list)
            
@task_app.route('/task/<id:id>/', method='PUT')
def update_tasks(id):
    tasks = db.tasks
    if request.json:
        taskdb = tasks.find_one({"_id": ObjectId(id)})
        if taskdb:
            updated_task = request.json
            tasks.update({"_id": ObjectId(id)}, 
            {'$set': {
                'done': updated_task['done'],
                'task': updated_task['task']
            }})
        else:
            abort(404, 'Task not found')
    else:
        abort(400, 'Error saving task')

@task_app.route('/task/<id:id>/', method='DELETE')
def delete_tasks(id):
    db.tasks.remove(id)

@task_app.route('/task/', method='POST')
def set_task():
    tasks = db.tasks
    if request.json:
        task_json = request.json
        if 'task' in task_json and 'done' in task_json:
            task_id = tasks.save(task_json)
            return dumps(str(task_id))
        else:
            abort(400, 'Error missing parameters')
    else:
        abort(400, 'Error saving task')
