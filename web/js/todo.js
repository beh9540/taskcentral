angular.module('todoApp', [])
  .controller('TodoController', ['$scope', '$http', '$log', function($scope, $http, $log) {
    $http.get('/task/').success(function(data){
      $scope.todos = data
    });
 
    $scope.addTodo = function() {
      $http.post('/task/', {task:$scope.todoText, done:false}).success(function(data){
        $scope.todos.push({_id: data, task:$scope.todoText, done:false});
        $scope.todoText = '';
      });
    };
 
    $scope.remaining = function() {
      var count = 0;
      angular.forEach($scope.todos, function(todo) {
        count += todo.done ? 0 : 1;
      });
      return count;
    };
    
    $scope.taskDone = function() {
      $http.put('/task/'+this.todo._id+"/", this.todo).success(function(data){
        $log.info('Task Done!');
      });
    };
    
    $scope.taskDelete = function(todo) {
      var todo_id = todo._id;
      $("#task"+todo_id).parent().remove();
      delete todo
      $http.delete('/task/'+todo_id+"/").success(function(data){
        $log.info('Task Deleted!')
      })
    }
 
    $scope.archive = function() {
      var oldTodos = $scope.todos;
      $scope.todos = [];
      angular.forEach(oldTodos, function(todo) {
        if (!todo.done) $scope.todos.push(todo);
      });
    };
  }]);